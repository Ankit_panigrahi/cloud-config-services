package com.lbg.paymentclient1;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RefreshScope
public class PaymentsClient1Application {

	public static Logger logger = LoggerFactory.getLogger(PaymentsClient1Application.class);

	public static final String UNAVAILABLE_SERVICE_STATUS = "payment service is temporarily unavailable ";

	public static void main(String[] args) {
		SpringApplication.run(PaymentsClient1Application.class, args);
	}

	@Autowired
	@Lazy
	public RestTemplate restTemplate;

	@Value("${appsign-service-url}")
	public String url;

	@Value("${payments.service.isEnabled}")
	public boolean status;

	@GetMapping("/getPayments")
	public List getNames() {

		if (status == true) {

			return restTemplate.getForObject(url + "/getPayments", List.class);
		} else {
			logger.info(UNAVAILABLE_SERVICE_STATUS);
			throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, UNAVAILABLE_SERVICE_STATUS);
		}

	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
