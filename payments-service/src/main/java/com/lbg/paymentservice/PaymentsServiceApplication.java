package com.lbg.paymentservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PaymentsServiceApplication {
	
	

	public static void main(String[] args) {
		SpringApplication.run(PaymentsServiceApplication.class, args);
	}
	
	@GetMapping("/getPayments")
	public List<String> getNames(){
		List<String> payments = new ArrayList<>();
		payments.add("Debit Card");
		payments.add("Credit Card");
		payments.add("Net Banking");
		payments.add("Cash");
		return payments;
	}

}
